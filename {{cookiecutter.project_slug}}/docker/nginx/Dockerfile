FROM {{ cookiecutter.nginx_image }}

USER 0

# key and value for the X-Frame-Options that overwrites what is used in
# nginx.conf
# see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
ARG X_FRAME_OPTIONS=""
ARG X_FRAME_OPTIONS_VALUE=""
ARG SERVER_NAME=""

# run a task with the deploy token
# RUN --mount=type=secret,id=build-secret,target=/run/secrets/build-secret/TOKEN

ADD conf /tmp/src/

{%- if cookiecutter.include_maintenance_site == "yes" %}
ADD maintenance /tmp/src/maintenance/
{%- endif %}

{%- if cookiecutter.include_error_pages == "yes" %}
ADD error_pages /tmp/src/error_pages/
{%- endif %}

RUN [ ${X_FRAME_OPTIONS} ] &&  \
  {%- if cookiecutter.include_maintenance_site == "yes" %}
  sed -i -e "s/^[[:space:]]*add_header X-Frame-Options .*/add_header X-Frame-Options ${X_FRAME_OPTIONS} ${X_FRAME_OPTIONS_VALUE};/" /tmp/src/nginx-cfg/maintenance.conf && \
  {%- endif %}
  {%- if cookiecutter.include_www_redirect == "yes" %}
  sed -i -e "s/^[[:space:]]*add_header X-Frame-Options .*/add_header X-Frame-Options ${X_FRAME_OPTIONS} ${X_FRAME_OPTIONS_VALUE};/" /tmp/src/nginx-cfg/redirect-www.conf && \
  {%- endif %}
  sed -i -e "s/^[[:space:]]*add_header X-Frame-Options .*/add_header X-Frame-Options ${X_FRAME_OPTIONS} ${X_FRAME_OPTIONS_VALUE};/" /tmp/src/nginx.conf || :

RUN [ ! -z "${SERVER_NAME}" ] &&  \
  {%- if cookiecutter.include_maintenance_site == "yes" %}
  sed -i -e "s/^[[:space:]]*server_name .*/server_name ${SERVER_NAME};/" /tmp/src/nginx-cfg/maintenance.conf && \
  {%- endif %}
  {%- if cookiecutter.include_www_redirect == "yes" %}
  sed -i -e "s/^[[:space:]]*server_name .*/server_name ${SERVER_NAME};/" /tmp/src/nginx-cfg/redirect-www.conf && \
  {%- endif %}
  sed -i -e "s/^[[:space:]]*server_name .*/server_name ${SERVER_NAME};/" /tmp/src/nginx.conf || :


RUN chown -R 1001:0 /tmp/src
USER 1001

# Let the assemble script to install the dependencies
RUN /usr/libexec/s2i/assemble

# Run script uses standard ways to run the application
CMD /usr/libexec/s2i/run
