# for handle maintenance web page
server {
    listen       8081;
    listen       [::]:8081;
    server_name  {{ cookiecutter.server_name }};
    client_body_timeout 10;
    client_header_timeout 10;
    client_max_body_size {{ cookiecutter.nginx_max_body_size }};

    add_header X-Frame-Options SAMEORIGIN always;
    add_header X-Content-Type-Options "nosniff" always;

    location @index {
        error_page 503 /maintenance.html;

        proxy_hide_header X-Powered-By;
        proxy_hide_header Server ;

        return 503;
    }

    # use RegExp because it has higher priority
    location / {
        root /opt/app-root/src/maintenance/;

        proxy_hide_header X-Powered-By;
        proxy_hide_header Server ;

        # redirect all URI to index (503) page (except asset URI),
        # asset URI still return status 200
        try_files $uri @index;
    }
}
